### Live Demo 🚀
A live demo can be found here https://p0werpuff.web.app/

### Setup & Run 🛠
Run `$ yarn` to install the dependencies and `$ yarn start` to run the developement server

### Built With 💻
*cue powerpuff girls intro voice*  

Sugar, spice, and everything nice  
These were the ingredients chosen  
To create the perfect little website  
But Professor Utonium accidentally  
Added an extra ingredient to the concoction--  
CHEMICAL X 🧪🧪🧪

Also the project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and includes [Redux Toolkit](https://github.com/reduxjs/redux-toolkit), an official library from the redux team,which dramatically reduces the boilerplate needed for Redux. It uses [React Router](https://reacttraining.com/react-router/) for routing and includes a couple of utility libraries like [lodash](https://lodash.com/), [axios](https://github.com/axios/axios) and [classnames](https://github.com/JedWatson/classnames).

### Style Guide 💅
The project follows [Airbnb's JavaScript Style Guide for React](https://airbnb.io/javascript/react/) and uses [eslint](https://eslint.org/) with [prettier](https://prettier.io/) to format the code. These dependencies can be installed by running `$ yarn --dev`

### Testing 🔬
More work needs to be done to increase coverage but the tests can be run using `$ yarn test`

### CI/CD 🚀
The project uses Gitlab CI for continuos integration and deployement. The `gitlab-ci.yml` file includes scripts that test and deploy the site when code is pushed to our master branch.

The environment variable `FIREBASE_TOKEN` needs to be set with a value [from firebase](https://firebase.google.com/docs/cli) in your CI environment.

The site can also be deployed by running `$ yarn deploy` from a terminal that's logged in to the firebase account.