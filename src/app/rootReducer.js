import { combineReducers } from '@reduxjs/toolkit';

import showReducer from '../features/show/showSlice';
import episodeReducer from '../features/episode/episodeSlice';
import searchReducer from '../features/search/searchSlice';

const rootReducer = combineReducers({
  show: showReducer,
  episode: episodeReducer,
  search: searchReducer,
});

export default rootReducer;
