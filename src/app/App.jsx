import React from 'react';
import {
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import Show from '../features/show/Show';
import Episode from '../features/episode/Episode';
import SearchInput from '../features/search/SearchInput';
import styles from './App.module.css';

function App() {
  return (
    <>
      <main className={styles.container}>
        <SearchInput />
        <Switch>
          <Route exact path="/shows/:showId" component={Show} />
          <Route exact path="/episodes/:episodeId" component={Episode} />
          <Route exact path="/">
            <Redirect to="/shows/6771" />
          </Route>
        </Switch>
      </main>
    </>
  );
}

export default App;
