/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { render, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import App from './App';
import configureStore from './configureStore';

afterEach(cleanup);

it('redirects / to our favorite show', async () => {
  const store = configureStore();
  const history = createMemoryHistory();
  history.push('/');

  render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>
    ,
  );

  expect(history.location.pathname).toBe('/shows/6771');
});
