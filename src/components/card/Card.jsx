import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Card.module.css';

const Card = ({ children, className }) => (
  <div className={classnames(styles.container, className)}>
    {children}
  </div>
);

Card.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Card.defaultProps = {
  className: '',
};

export default Card;
