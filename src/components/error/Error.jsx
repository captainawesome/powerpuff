import React from 'react';
import PropTypes from 'prop-types';
import ErrorImg from '../../assets/error.svg';
import styles from './Error.module.css';

const Error = ({ onRetry }) => (
  <div className={styles.error}>
    <img className={styles.image} src={ErrorImg} alt="error" />
    <h1>Something went wrong</h1>
    <button className={styles.button} type="button" onClick={onRetry}>
      Try Again
    </button>
  </div>
);

Error.propTypes = {
  onRetry: PropTypes.func.isRequired,
};

export default Error;
