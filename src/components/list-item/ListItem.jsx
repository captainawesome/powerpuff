import React from 'react';
import PropTypes from 'prop-types';
import styles from './ListItem.module.css';

const ListItem = ({
  image, title, subtitle,
}) => (
  <div className={styles.container}>
    <img className={styles.image} src={image} alt="avatar" />
    <div className={styles.details}>
      <p className={styles.title}>{title}</p>
      <p className={styles.subtitle}>{subtitle}</p>
    </div>
  </div>
);

ListItem.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
};

ListItem.defaultProps = {
  image: 'https://via.placeholder.com/150',
};

export default ListItem;
