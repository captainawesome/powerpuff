import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import search from '../../assets/search.svg';
import useDebounce from '../../hooks/useDebounce';
import SearchResults from './SearchResults';
import styles from './SearchInput.module.css';
import { searchShows, clearSearch } from './searchSlice';

const CHARRACTER_TRESHOLD = 2;
const DEBOUNCE_MILLISECONDS = 1000;

function SearchInput() {
  const [debouncedQuery, query, setQuery] = useDebounce('', DEBOUNCE_MILLISECONDS);
  const results = useSelector((state) => state.search.results);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    const text = e.target.value;
    if (query.length === CHARRACTER_TRESHOLD + 1
        && text.length === CHARRACTER_TRESHOLD) {
      // clear results for eg. when query changes from 3 to 2 characters
      dispatch(clearSearch());
    }
    setQuery(text);
  };

  useEffect(() => {
    if (debouncedQuery.length > CHARRACTER_TRESHOLD) {
      dispatch(searchShows(debouncedQuery));
    }
  }, [dispatch, debouncedQuery]);

  return (
    <div className={styles.container}>
      <img className={styles.icon} src={search} alt="search" />
      <input
        className={styles.input}
        placeholder="search..."
        type="text"
        value={query}
        onChange={handleChange}
      />
      {(results.length > 0 && query.length > CHARRACTER_TRESHOLD) && (
        <SearchResults />
      )}
    </div>
  );
}

export default SearchInput;
