/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import { search } from '../../api/tvmazeAPI';

const initialState = {
  results: [],
  loading: false,
  error: null,
};

const showSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    clearSearch(state) {
      state.results = [];
      state.loading = false;
      state.error = null;
    },
    searchShowsStart(state) {
      state.loading = true;
      state.error = null;
    },
    searchShowsSuccess(state, action) {
      const { results } = action.payload;
      state.results = results;
      state.loading = false;
      state.error = null;
    },
    searchShowsFailure(state, action) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  clearSearch,
  searchShowsStart,
  searchShowsSuccess,
  searchShowsFailure,
} = showSlice.actions;
export default showSlice.reducer;

export const searchShows = (query) => async (dispatch) => {
  try {
    dispatch(searchShowsStart());
    const results = await search(query);
    dispatch(searchShowsSuccess({ results }));
  } catch (err) {
    dispatch(searchShowsFailure(err.message));
  }
};
