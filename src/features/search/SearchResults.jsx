import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import dayjs from 'dayjs';
import Card from '../../components/card/Card';
import ListItem from '../../components/list-item/ListItem';
import styles from './SearchResults.module.css';
import { clearSearch } from './searchSlice';

function SearchResults() {
  const results = useSelector((state) => state.search.results);
  const dispatch = useDispatch();

  return (
    <Card className={styles.container}>
      {results.map((res) => (
        <Link to={`/shows/${res.show.id}`} onClick={() => dispatch(clearSearch())} key={res.show.id}>
          <ListItem
            image={res.show.image?.medium}
            title={res.show.name}
            subtitle={dayjs(res.show.premiered).format('MMM DD, YYYY')}
          />
        </Link>
      ))}
    </Card>
  );
}

export default SearchResults;
