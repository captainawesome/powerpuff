import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import DetailHeader from './DetailHeader';
import EpisodeDetail from './EpisodeDetail';
import Card from '../../components/card/Card';
import Spinner from '../../components/spinner/Spinner';
import Error from '../../components/error/Error';
import { fetchEpisode, episodeSelector, showSelector } from './episodeSlice';
import styles from './Episode.module.css';


function Episode() {
  const { episodeId } = useParams();
  const dispatch = useDispatch();

  const {
    loading, error, episode, show,
  } = useSelector(
    (state) => ({
      loading: state.episode.loading,
      error: state.episode.error,
      episode: episodeSelector(state, episodeId),
      show: showSelector(state),
    }),
  );

  useEffect(() => {
    if (!episode) dispatch(fetchEpisode(episodeId));
  }, [dispatch, episode, episodeId]);

  return (
    <>
      {loading && <Spinner />}
      {error && <Error onRetry={() => dispatch(fetchEpisode(episodeId))} />}
      {episode && (
        <Card className={styles.container}>
          <DetailHeader showId={show.id} />
          <EpisodeDetail episodeId={episodeId} />
        </Card>
      )}
    </>
  );
}

export default Episode;
