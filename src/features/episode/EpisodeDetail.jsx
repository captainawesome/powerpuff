/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import styles from './EpisodeDetail.module.css';
import { episodeSelector, showSelector } from './episodeSlice';

const EpisodeDetail = ({ episodeId }) => {
  const episode = useSelector((state) => episodeSelector(state, episodeId));
  const show = useSelector(showSelector);

  return (
    <div className={styles.container}>
      <img
        className={styles.image}
        src={episode.image?.original ?? show.image?.original}
        alt={episode.name}
      />
      <div className={styles.details}>
        <h1 className={styles.name}>{episode.name}</h1>
        <p className={styles.description}>{episode.summary?.replace(/(<([^>]+)>)/ig, '') ?? 'No episode summary available'}</p>
      </div>
    </div>
  );
};

EpisodeDetail.propTypes = {
  episodeId: PropTypes.number.isRequired,
};

export default EpisodeDetail;
