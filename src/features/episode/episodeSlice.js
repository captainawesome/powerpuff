/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import { getEpisodesKeyedById } from '../show/showSlice';
import { getEpisode } from '../../api/tvmazeAPI';

const initialState = {
  episode: null,
  loading: false,
  error: null,
};

const showSlice = createSlice({
  name: 'episode',
  initialState,
  reducers: {
    fetchEpisodeStart(state) {
      state.loading = true;
      state.error = null;
    },
    fetchEpisodeSuccess(state, action) {
      const { episode } = action.payload;
      state.episode = episode;
      state.loading = false;
      state.error = null;
    },
    fetchEpisodeFailure(state, action) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  fetchEpisodeStart,
  fetchEpisodeSuccess,
  fetchEpisodeFailure,
} = showSlice.actions;
export default showSlice.reducer;

export const fetchEpisode = (showId) => async (dispatch) => {
  try {
    dispatch(fetchEpisodeStart());
    const episode = await getEpisode(showId);
    dispatch(fetchEpisodeSuccess({ episode }));
  } catch (err) {
    dispatch(fetchEpisodeFailure(err.message));
  }
};

export const episodeSelector = (state, episodeId) => getEpisodesKeyedById(state)[episodeId] ?? state.episode.episode;
export const showSelector = (state) => state.show?.show || state.episode?.episode?._embedded.show;
