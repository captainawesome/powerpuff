import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import LeftIcon from '../../assets/left.svg';
import styles from './DetailHeader.module.css';

const DetailHeader = ({ showId }) => (
  <div className={styles.header}>
    <Link className={styles.title} to={`/shows/${showId}`}>
      <img className={styles.icon} src={LeftIcon} alt="back" />
      <p>Back to show</p>
    </Link>
  </div>
);

DetailHeader.propTypes = {
  showId: PropTypes.number.isRequired,
};

export default DetailHeader;
