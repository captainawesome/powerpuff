/* eslint-disable no-param-reassign */
import { createSlice, createSelector } from '@reduxjs/toolkit';
import { keyBy, groupBy } from 'lodash';
import { getShow } from '../../api/tvmazeAPI';

const initialState = {
  show: null,
  loading: true,
  error: null,
  selectedSeason: 1,
};

const showSlice = createSlice({
  name: 'show',
  initialState,
  reducers: {
    selectSeason(state, action) {
      state.selectedSeason = action.payload.selectedSeason;
    },
    fetchShowStart(state) {
      state.show = null;
      state.loading = true;
      state.error = null;
    },
    fetchShowSuccess(state, action) {
      const { show } = action.payload;
      state.show = show;
      state.loading = false;
      state.error = null;
      state.selectedSeason = 1;
    },
    fetchShowFailure(state, action) {
      state.show = null;
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  selectSeason,
  fetchShowStart,
  fetchShowSuccess,
  fetchShowFailure,
} = showSlice.actions;
export default showSlice.reducer;

export const fetchShow = (showId) => async (dispatch) => {
  try {
    dispatch(fetchShowStart());
    const show = await getShow(showId);
    dispatch(fetchShowSuccess({ show }));
  } catch (err) {
    dispatch(fetchShowFailure(err.message));
  }
};

const episodesSelector = (state) => state.show.show?._embedded.episodes;

export const getEpisodesKeyedById = createSelector(
  episodesSelector,
  (episodes) => keyBy(episodes, 'id'),
);

export const getEpisodesGroupedBySeason = createSelector(
  episodesSelector,
  (episodes) => groupBy(episodes, 'season'),
);

export const getSeasons = (state) => Object.keys(getEpisodesGroupedBySeason(state));
export const getSelectedSeason = (state) => state.show.selectedSeason;
