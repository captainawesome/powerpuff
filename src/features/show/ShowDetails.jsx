import React from 'react';
import { useSelector } from 'react-redux';
import Card from '../../components/card/Card';
import styles from './ShowDetails.module.css';

function ShowDetails() {
  const show = useSelector((state) => state.show.show);

  return (
    <Card className={styles.container}>
      <img className={styles.image} src={show.image?.original} alt={show.name} />
      <div className={styles.details}>
        <h1 className={styles.name}>{show.name}</h1>
        <p className={styles.rating}>{show.rating?.average}</p>
        <p data-testid="summary" className={styles.description}>
          {show.summary?.replace(/(<([^>]+)>)/ig, '') ?? 'No show summary available'}
        </p>
      </div>
    </Card>
  );
}

export default ShowDetails;
