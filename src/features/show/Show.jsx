import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import EpisodeList from './EpisodeList';
import Spinner from '../../components/spinner/Spinner';
import Error from '../../components/error/Error';
import { fetchShow } from './showSlice';
import ShowDetails from './ShowDetails';

function Show() {
  const { showId } = useParams();
  const dispatch = useDispatch();

  const { loading, error, show } = useSelector(
    (state) => ({
      loading: state.show.loading,
      error: state.show.error,
      show: state.show.show,
    }),
  );

  // call fetch if the show id available in state is different from the showId passed in params
  useEffect(() => {
    if (Number(showId) !== show?.id) dispatch(fetchShow(showId));
  }, [dispatch, show, showId]);

  return (
    <>
      {loading && <Spinner />}
      {error && <Error onRetry={() => dispatch(fetchShow(showId))} />}
      {show && (
        <>
          <ShowDetails />
          <EpisodeList />
        </>
      )}
    </>
  );
}

export default Show;
