import React from 'react';
import { useSelector } from 'react-redux';
import EpisodeListItem from './EpisodeListItem';
import EpisodeListHeader from './EpisodeListHeader';
import styles from './EpisodeList.module.css';
import { getEpisodesGroupedBySeason, getSelectedSeason } from './showSlice';

function EpisodeList() {
  const episodesBySeason = useSelector(getEpisodesGroupedBySeason);
  const selectedSeason = useSelector(getSelectedSeason);

  return (
    <div className={styles.container}>
      <EpisodeListHeader />
      {episodesBySeason[selectedSeason].map((episode) => (
        <EpisodeListItem key={episode.id} episode={episode} />
      ))}
    </div>
  );
}

export default EpisodeList;
