import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './EpisodeListHeader.module.css';
import { getSeasons, getSelectedSeason, selectSeason } from './showSlice';

function EpisodeListHeader() {
  const seasons = useSelector(getSeasons);
  const selectedSeason = useSelector(getSelectedSeason);
  const dispatch = useDispatch();

  return (
    <div className={styles.header}>
      <h1 className={styles.title}>Episodes</h1>
      <select
        className={styles.picker}
        value={selectedSeason}
        onChange={(e) => dispatch(selectSeason({ selectedSeason: e.target.value }))}
      >
        {seasons.map((s) => (
          <option key={s} value={s}>{`Season ${s}`}</option>
        ))}
      </select>
    </div>
  );
}

export default EpisodeListHeader;
