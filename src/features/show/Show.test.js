/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import axiosMock from 'axios';
import {
  render, cleanup, waitForElement, waitForDomChange,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import configureStore from '../../app/configureStore';
import Show from './Show';

afterEach(cleanup);
jest.mock('axios');

// mock api response
const res = {
  data:
    {
      id: 6771,
      name: 'The Powerpuff Girls',
      rating: { average: 5.9 },
      image: { medium: 'http://static.tvmaze.com/uploads/images/medium_portrait/60/151357.jpg', original: 'http://static.tvmaze.com/uploads/images/original_untouched/60/151357.jpg' },
      summary: "<p>The city of Townsville may be a beautiful, bustling metropolis, but don't be fooled! There's evil afoot! And only three things can keep the bad guys at bay: Blossom, Bubbles and Buttercup, three super-powered little girls, known to their fans (and villains everywhere) as <b>The Powerpuff Girls</b>.</p>",
      _embedded: {
        episodes: [{
          id: 657308, name: 'Escape from Monster Island', season: 1, number: 1, airdate: '2016-04-04', image: { medium: 'http://static.tvmaze.com/uploads/images/medium_landscape/53/132617.jpg', original: 'http://static.tvmaze.com/uploads/images/original_untouched/53/132617.jpg' }, summary: "<p>Bubbles wins two tickets to a concert and has to pick between Blossom and Buttercup to go with her. Meanwhile, the Mayor's plane goes down over Monster Island and it's up to the girls to rescue him.</p>",
        }],
      },
    }
  ,
};

function renderWithRouter() {
  const store = configureStore();
  const history = createMemoryHistory();
  history.push('/shows/6771');

  const rendered = render(
    <Provider store={store}>
      <Router history={history}>
        <Route path="/shows/:showId">
          <Show />
        </Route>
      </Router>
    </Provider>,
  );
  return { history, ...rendered };
}

it('renders loading and fetches data', async () => {
  axiosMock.get.mockResolvedValueOnce(res);
  const { getByText } = renderWithRouter();

  expect(getByText(/loading/i)).toBeInTheDocument(); // renders loading text
  expect(axiosMock.get).toHaveBeenCalledWith('https://api.tvmaze.com/shows/6771?embed=episodes');
  expect(axiosMock.get).toHaveBeenCalledTimes(1);// component makes an api request
});

it('displays show details', async () => {
  axiosMock.get.mockResolvedValueOnce(res);
  const { getByText, getByTestId, getByAltText } = renderWithRouter();

  await waitForDomChange();
  expect(getByAltText('The Powerpuff Girls').src).toBe('http://static.tvmaze.com/uploads/images/original_untouched/60/151357.jpg'); // renders the original image
  expect(getByText('The Powerpuff Girls')).toBeInTheDocument(); // render the show title
  expect(getByText('5.9')).toBeInTheDocument(); // renders the rating
  expect(getByTestId('summary')).not.toContain('<p>'); // renders the summary with the html tags stripped
});

it('displays the episode list and season picker', async () => {
  axiosMock.get.mockResolvedValueOnce(res);
  const { getByText } = renderWithRouter();

  await waitForDomChange();
  expect(getByText('Episodes')).toBeInTheDocument();
  expect(getByText('Season 1')).toBeInTheDocument();
  expect(getByText('1 - Escape from Monster Island')).toBeInTheDocument();
});

it('navigates to episode detail page when an episode is clicked', async () => {
  axiosMock.get.mockResolvedValueOnce(res);
  const { history, getAllByTestId } = renderWithRouter();

  await waitForDomChange();
  const el = getAllByTestId('item');
  userEvent.click(el[0]);// simulate click on an episode
  expect(history.location.pathname).toBe('/episodes/657308');
});

it('renders error', async () => {
  axiosMock.get.mockRejectedValueOnce(new Error('error'));// simulate error response from server
  const { getByText } = renderWithRouter();

  const errorElement = await waitForElement(() => getByText(/something went wrong/i));
  expect(errorElement).toBeInTheDocument();// expect error to be rendered
});
