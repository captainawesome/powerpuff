import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import dayjs from 'dayjs';
import Card from '../../components/card/Card';
import ListItem from '../../components/list-item/ListItem';

function EpisodeListItem({ episode }) {
  const showImage = useSelector((state) => state.show.show.image);

  return (
    <Link data-testid="item" to={`/episodes/${episode.id}`} key={episode.id}>
      <Card>
        <ListItem
          image={episode.image?.medium ?? showImage?.medium}
          title={`${episode.number} - ${episode.name}`}
          subtitle={dayjs(episode.airdate).format('MMM DD, YYYY')}
        />
      </Card>
    </Link>
  );
}

EpisodeListItem.propTypes = {
  episode: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    season: PropTypes.number,
    number: PropTypes.number,
    airdate: PropTypes.string,
    image: PropTypes.shape({
      medium: PropTypes.string,
      original: PropTypes.string,
    }),
  }).isRequired,
};

export default EpisodeListItem;
