import axios from 'axios';

const ROOT_URL = 'https://api.tvmaze.com';

export async function search(query) {
  const url = `${ROOT_URL}/search/shows?q=${query}`;
  const { data } = await axios.get(url);
  return data;
}

export async function getShow(showId) {
  const url = `${ROOT_URL}/shows/${showId}?embed=episodes`;
  const { data } = await axios.get(url);
  return data;
}

export async function getEpisode(episodeId) {
  const url = `${ROOT_URL}/episodes/${episodeId}?embed=show`;
  const { data } = await axios.get(url);
  return data;
}
